import copy
import math
import pandas as pd
import pptree as ppt
import numpy as np

# Leer los datos desde el archivo CSV
data_path = 'C:/Users/zapan/Desktop/Tornillo/docs/ferreteria.csv'  # ruta del archivo
data = pd.read_csv(data_path)
print(data)
data = data.drop(columns='NombreProducto')  # Elimina columna 'NombreProducto'

# Función de entropía
def entropy(data: pd.DataFrame) -> float:
    decision_classes = data.groupby('EsExclusivoEnLínea').count()
    e = 0
    for c in decision_classes.iloc[:, 0]:
        p = c / len(data)
        e += (-1) * math.log2(p) * p
    return e

# Función de entropía condicional
def conditional_entropy(data: pd.DataFrame, col_name: str):
    data_reduced = data[[col_name, 'EsExclusivoEnLínea']]
    var_classes = data_reduced.groupby(col_name).count()
    e = 0
    for idx, row in var_classes.iterrows():
        p = row['EsExclusivoEnLínea'] / len(data)
        data_filtered = data_reduced[data_reduced.loc[:, col_name] == idx]
        e += p * entropy(data_filtered)
    return e

# Función de ganancia después de elegir la variable A
def gain(data: pd.DataFrame, column_name: str):
    e_before = entropy(data)
    e_after = conditional_entropy(data, column_name)
    return e_before - e_after

# Función intrínseca
def intrinsicInfo(data: pd.DataFrame, column_name: str):
    data_reduced = data[[column_name, 'EsExclusivoEnLínea']]
    var_classes = data_reduced.groupby(column_name).count()
    e = 0
    for idx, row in var_classes.iterrows():
        p = row.iloc[0] / len(data_reduced)
        if p > 0:  # Para evitar la división por cero (si esque hay algun valor)
            e += (-1) * p * math.log2(p)
    return e

# Función de relación de ganancia
def gainRatio(data: pd.DataFrame, column_name: str):
    gain_val = gain(data, column_name)
    intrinsic_info_val = intrinsicInfo(data, column_name)
    
    if intrinsic_info_val == 0 or np.isnan(intrinsic_info_val):
        return 0  # Evitar división por cero o NaN
    
    return gain_val / intrinsic_info_val

# Función para dividir el atributo continuo 'Precio' en dos partes
def age_spliter(data: pd.DataFrame):
    data_reduced = data[['Precio', 'EsExclusivoEnLínea']]
    data_reduced = data_reduced.sort_values('Precio')
    divider_pos = 1
    best_entropy = 1
    best_pos = 1
    while divider_pos < len(data):
        el = (divider_pos / len(data)) * entropy(data_reduced.iloc[:divider_pos])
        ep = (len(data) - divider_pos) / len(data) * entropy(data_reduced.iloc[divider_pos:])
        total = el + ep
        if total < best_entropy:
            best_entropy = total
            best_pos = divider_pos
        divider_pos += 1
    age = data_reduced.iloc[best_pos, 0]
    data_copy = copy.deepcopy(data)
    data_copy['Precio'] = data_copy['Precio'].map(lambda x: '<' + str(age) if x < age else '>=' + str(age))
    return gainRatio(data_copy, 'Precio'), best_pos

# Función para crear el árbol de decisión
def create_tree(data: pd.DataFrame, available_attr: list) -> dict:
    if entropy(data) == 0:
        return list(data.EsExclusivoEnLínea)[0]
    best_attr = available_attr[0]
    best_gain = 0
    local_tree = {}

    for attr in available_attr:
        if attr == 'IDProducto':
            continue
        if attr == 'Precio':
            gain = age_spliter(data)[0]
        else:
            gain = gainRatio(data, attr)
        if gain > best_gain:
            best_gain = gain
            best_attr = attr

    if best_attr == 'Precio':
        data = data.sort_values('Precio')
        splitter = age_spliter(data)[1]
        age = data.iloc[splitter, :]['Precio']
        available_attr.remove(best_attr)
        sub_tree1 = create_tree(data[:splitter], copy.deepcopy(available_attr))
        local_tree['(precio < ' + str(age) + ')'] = sub_tree1
        sub_tree2 = create_tree(data[splitter:], copy.deepcopy(available_attr))
        local_tree['(precio >= ' + str(age) + ')'] = sub_tree2

    else:
        data_reduced = data[[best_attr, 'EsExclusivoEnLínea']]
        var_classes = data_reduced.groupby(best_attr).count()
        available_attr.remove(best_attr)

        for aClass, _ in var_classes.iterrows():
            sub_tree = create_tree(
                data[data.loc[:, best_attr] == aClass],
                copy.deepcopy(available_attr)
            )
            local_tree[str(aClass) + '(' + best_attr + ')'] = sub_tree

    return local_tree

# Función para realizar el recorrido BFS y transformar el árbol en una estructura comprensible para pptree
def bfs(tree: dict, this_node: ppt.Node) -> ppt.Node:
    if not isinstance(tree, dict):
        return ppt.Node(tree, this_node)
    for key in tree.keys():
        next_node = ppt.Node(key, this_node)
        bfs(tree[key], next_node)
    return this_node

# Crear el árbol
tree = create_tree(data, list(data.columns[:-1]))

# Imprimir el árbol utilizando pptree
root_node = ppt.Node('Root', None)
ppt_tree = bfs(tree, root_node)
ppt.print_tree(ppt_tree)
